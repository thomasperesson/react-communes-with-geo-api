export const getCommuneByName = (name) => {
    const apiUrl = `https://geo.api.gouv.fr/communes?nom=${name}&fields=code,nom,departement,region,population,codesPostaux&limit=10`;
    return fetch(apiUrl)
        .then(response => {
            if(!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(() => {
            return 'Erreur';
        })
}