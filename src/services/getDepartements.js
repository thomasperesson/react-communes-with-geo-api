const getDepartements = () => {
    const apiUrl = `https://geo.api.gouv.fr/departements/`;
    return fetch(apiUrl)
        .then(response => {
            if(!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(() => {
            return 'Erreur';
        })
}

export default getDepartements;