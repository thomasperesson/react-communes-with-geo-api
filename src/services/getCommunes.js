const getCommunes = (dep = "01") => {
    const apiUrl = `https://geo.api.gouv.fr/departements/${dep}/communes?fields=code,nom,departement,region,population,codesPostaux`;
    return fetch(apiUrl)
        .then(response => {
            if(!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(() => {
            return 'Erreur';
        })
}

export default getCommunes;