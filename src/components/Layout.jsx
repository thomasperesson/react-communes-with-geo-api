const Layout = () => {
    return (
        <main>
            <Header />
            <Outlet />
            <Footer />
        </main>
        
    )
}

export default Layout;