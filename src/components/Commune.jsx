import { Link } from "react-router-dom";

const Commune = ({...commune}) => {
    return (
        <ul>
            <li><Link to={`/commune/${commune.code}`} state={{commune: {...commune}}}>{commune.nom}</Link></li>
            <li>
                Code postal :
                <ul>
                    {commune.codesPostaux.map((code, i) => {
                        return (
                            <li key={i}>{code}</li>
                        );
                    })}
                </ul>
            </li>
        </ul>
    );
};

export default Commune;