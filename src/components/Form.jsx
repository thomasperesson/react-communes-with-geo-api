import { useEffect, useState } from "react";
import getDepartements from "../services/getDepartements";

const Form = ({handleChangeDep, handleChangeName}) => {

    const [deps, setDeps] = useState([]);

    useEffect(() => {
        getDepartements().then(data => {
            setDeps(data);
        })
    }, []);


    return (
        <form action="">
            <select onChange={(e, dep) => {handleChangeDep(e, dep)}} name="deps" id="deps">
                {deps.map((dep, index) => {
                    return (
                        <option key={index} value={dep.code}>{dep.code} - {dep.nom}</option>
                    )
                })}
            </select>
            <input type="text" onKeyUp={(e, name) => {handleChangeName(e, name)}} />
        </form>
    )
}

export default Form;