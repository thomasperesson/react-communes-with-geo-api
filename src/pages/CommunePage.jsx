import { Link, useLocation } from "react-router-dom";

const CommunePage = () => {
    const location = useLocation();
    const cityName = location.state?.commune.nom;
    const population = location.state?.commune.population;
    const codesPostaux = location.state?.commune.codesPostaux;

    return (
        <>
            <h1>Détails pour la commune : {cityName && cityName}</h1>
            Code postal :
            <ul>
                {codesPostaux.map((code, i) => {
                    return (
                        <li key={i}>{code}</li>
                    );
                })}
            </ul>
            <p>Population : {population} habs</p>
            <Link to="/">Retour à la liste des communes</Link>
        </>
    );
};

export default CommunePage;