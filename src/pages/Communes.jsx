import { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import getCommunes from '../services/getCommunes';
import Form from '../components/Form';
import Commune from '../components/Commune';
import { getCommuneByName } from '../services/getCommuneByName';

const Communes = () => {
    
    const [communes, setCommunes] = useState([]);
    const [dep, setDep] = useState("01");
    const [name, setName] = useState("");

    useEffect(() => {
        
        if(name === "") {
            getCommunes(dep).then(data => {
                setCommunes(data);
            })
        } else {
            getCommuneByName(name).then(data => {
                setCommunes(data);
            })
        }
        
        
    }, [dep, name]);

    const handleChangeDep = (e) => {
        const newDep = e.target.value;
        setDep(newDep);
    }

    const handleChangeName = (e) => {
        const newName = e.target.value;
        setName(newName);
    }

    return (
        <>
            <h1>Liste des Communes</h1>
            <Form handleChangeName={(e, name) => {handleChangeName(e, name)}} handleChangeDep={(e, dep) => {handleChangeDep(e, dep)}} />
            {communes.map((commune, index) => {
                return (
                    <Commune key={index} {...commune} />
                )
            })}
        </>
        
    )
}

export default Communes;